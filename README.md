# Programação Orientada a Objeto em Python

01. [Classes](https://bitbucket.org/carlos_oberdan/python-exemplos/src/master/#01-classes)
02. [Instances, Instance methods, Instance attributes](#02-instances-instance-methods-instance-attributes)
03. [Class attributes](#03-class-attributes)
04. [The __init__ constructor](#04-the-init-constructor)
05. [Inheritance (Inheriting {attributes,methods,constructors etc..})](#05-inheritance-inheriting-attributesmethodsconstructors-etc)
06. [Encapsulation](#06-encapsulation)
07. [Polymorphism](#07-polymorphism)
08. [Instance methods](#08-instance-methods)
09. [Multiple Inheritance and method/attribute lookup](#10-multiple-inheritance-and-how-lookup-works)
10. [Method Resolution Order (MRO)](#11-method-resolution-order-mro)
11. [Decorators](#12-decorators)
12. [Static methods](#13-static-methods)
13. [Class methods](#14-class-methods)


# NOTAS
------------
#### 01. Classes

Classes são os blocos básicos utilizados em POO. 

São uma espécie de "template" utilizadas para criar instâncias (objetos). 

------------
#### 02. Instâncias, métodos de instância e atributos de instância

------------
#### 03. Atributos de classes

São métodos ou atributos que pertencem a classe e não a uma instância. 

Existe uma única cópia

Exemplo:

```
class MinhaClasse(object):
    valor = 10
    
    def __init__(self):
        pass
```

`valor` é um atributo de classe. 

------------
#### 04.  O método __init__ 

O método __init__() é usado como construtor quando um objeto é instanciado.

Qualquer atributos definido dentro de  __init__() será criado no momento da criação (instanciação) do objeto.

------------
#### 05. Herança

-------------
#### 06. Encapsulamento

Getters e setters são usados​em muitas linguagens de programação orientada a objetos para garantir o princípio do encapsulamento de dados. 
O encapsulamento de dados é visto como o agrupamento de dados com os métodos que operam nesses dados. 
Esses métodos são, obviamente, o `getter` para recuperar os dados e o `setter` para alterar os dados.
 
De acordo com esse princípio, os atributos de uma classe são tornados privados para ocultá-los e protegê-los de outro código.



------------
#### 07. Polimorfismo

------------
#### 08. Métodos de instância

------------
#### 09. Herança múltipla

* Any class can inherit from other classes.
* Any python class can inherit from multiple classes at the same time.
* The class that inherits another class is called the Base/Child class.
* The class being inherited by the Child class is called the Parent class.
* The child class inherits any methods and attributes defined in the parent classes.
* Python uses a depth-first method resolution order (MRO) to fetch methods.
* When two classes inherits from the same class, from Python2.3 onwards, the MRO omits the first occurrence of the class.
* This new MRO lookup method applies from Python2.3, and is for the new-style classes.
	*NOTE:* New style classes inherits from the 'object' parent class.

------------
#### 10. Ordem de resolução de métodos (MRO - Method Resolution Order)

* Python has a method lookup order, called `MRO` (Method Resolution Order)
* The MRO path can be printed to stdout using `print <class-name>.mro()`
* Python, by default, uses a depth-first lookup path for MRO.

* ie.. Imagine you have four classes, A, B, C, D.

  1. You instance is created from `D`.
  2. `D` inherits from `B` and `C`
  3. `B` inherits from `A`.
  4. Both `C` and `A` has a method with the same name.
  5. Since python follows a depth-first MRO, the method is called from `A`

REFERENCE: Check the code examples in:
  * 14-multiple-inheritance-1.py
  * 15-multiple-inheritance-2.py

In some cases, the inheritance can get quite cumbersome when multiple classes inherit from the same classes, in multiple levels. 

**NOTE** : From Python2.3, the MRO has changed slightly in order to speed up the method lookups.

The MRO lookup now skips/omits the initial occurrences of classes which occurs multiple time in the lookup path.

* Example:
  1. Four classes, A, B, C, D.
  2. D inherits from both B and C
  3. B inherits from A
  4. C inherits from A
  5. Both C and A contains a similar named method.
  6. Your instance in created from class D.
  7. You try a lookup for the method which is named both in A and C.
  8. The usual lookup should be D -> B -> A -> C -> A
  	a. Hence since the method exists in A and C, it should return from A.
  	b. But since the class A is occurring twice and due to the new MRO method, the lookup will be 
  	D -> B -> C -> A.
  9. The lookup should return the method from class C, rather than A.

REFERENCE: Check the code example in:
  * 16-multiple-inheritance-3.py

--------------
#### 11. Decorators

--------------
#### 12. Métodos estáticos

--------------
#### 13. Métodos de classe

--------------

#### 14. Magic methods

Magic methods 



